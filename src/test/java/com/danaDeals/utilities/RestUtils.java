package com.danaDeals.utilities;

import org.apache.commons.lang3.RandomStringUtils;

public class RestUtils {

    public static String nameGenerator() {
        String capital = RandomStringUtils.randomAlphabetic(1).toUpperCase();
        String generateString = RandomStringUtils.randomAlphabetic(8);
        String generateString1 = RandomStringUtils.randomAlphabetic(10);
        generateString = generateString1+" "+ generateString;
        return generateString+capital;
    }

    public static String gmailGenerator() {
        String generateString1 = RandomStringUtils.randomAlphabetic(50);
        String generateString2 = RandomStringUtils.randomAlphabetic(19);
        String domain = RandomStringUtils.randomAlphabetic(3);
        String gmail = generateString1 + "@"+ generateString2+"."+domain;
        return gmail;
    }

    public static String passwordGenerator() {
        String capital = RandomStringUtils.randomAlphabetic(1).toUpperCase();
        String lowercase=RandomStringUtils.randomAlphabetic(13).toLowerCase();
        int number = Integer.parseInt(RandomStringUtils.randomNumeric(1));
        return "@"+number+capital+lowercase;
    }

    public static String telephoneNumber() {
        String telephone = RandomStringUtils.randomNumeric(9);
        telephone = "081"+telephone;
        return telephone;
    }

    public static String changeTelephone(String telephone) {
        String subTelephone = telephone.substring(0,3);
        String subTelephone2 = telephone.substring(2,4);
        String toBeReplaced = telephone.substring(0, 4);
        if(subTelephone.contains("08"))
        {
            telephone = telephone.replace(toBeReplaced, "+628"+subTelephone2);
            return  telephone;
        }
        else
            telephone.replace(toBeReplaced, "08");
        return telephone;
    }

    public static String virtualNumberGenerator(String telephone,String merchant){
        if(merchant.equals("bni"))
        {
            String virtualNumber = "9030"+telephone;
            return virtualNumber;
        }
        else if(merchant.equals("mandiri"))
        {
            String virtualNumber = "2372"+telephone;
            return virtualNumber;
        }
        else if(merchant.equals("bca"))
        {
            String virtualNumber = "4354"+telephone;
            return virtualNumber;
        }
        String virtualNumber = "3357"+telephone;
        return virtualNumber;
    }

    public static String voucherNameGenerator() {
        String generateString = RandomStringUtils.randomAlphabetic(6);
        return "Voucher " + generateString;
    }
}
