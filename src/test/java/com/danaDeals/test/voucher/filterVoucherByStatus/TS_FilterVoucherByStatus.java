package com.danaDeals.test.voucher.filterVoucherByStatus;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.Utility;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_FilterVoucherByStatus extends TestBase {

    @BeforeClass
    private void loggerInfo() {
        logger.info("********" +getClass().getName()+ "*********");
    }

    @Test(dataProvider = "filterByStatusData")
    private void TC_FilterByStatus(String tcID, String status, String page, String devStatus, String message,
                                   String httpStatus, String path) {

        String loginToken = loadFile("adminToken.txt");
        String invalidToken = loadFile("invalidToken.txt");

        logger.info("******** TC_FilterVoucherByStatus_" + tcID + " ********");

        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        if (Integer.parseInt(tcID) == 3 || Integer.parseInt(tcID) == 4) {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/admin/voucher/filterByStatus?filterByStatus="+status);

        } else if (Integer.parseInt(tcID) == 8) {
            httpRequest.header("Authorization", "Bearer " + invalidToken);
            response = httpRequest.request(Method.GET, "/api/admin/voucher/filterByStatus?filterByStatus="+status+"&"+page);

        } else if (Integer.parseInt(tcID) == 1 || Integer.parseInt(tcID) == 2) {

            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/admin/voucher/filterByStatus?filterByStatus="+status+"&"+page);

            //Check if in one page only have maximum 10 vouchers
            checkVoucherInAPage(getNumberOfVoucher());

            //Check if the vouchers that collected is have a correct status
            for (int i = 0; i < getNumberOfVoucher(); i++) {
                checkDataEquals("data.content.status[0]["+i+"]", status);
            }

        } else {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/admin/voucher/filterByStatus?filterByStatus="+status+"&"+page);

        }

        responseBody = response.getBody().asString();

        //check response body
        checkBody(message);
        checkPath(path);
        checkTimestamp();

        //check status
        checkDevStatus(devStatus);
        checkStatusCode(httpStatus);

        //check response time
        checkResponseTime("3000");
    }

    @DataProvider(name = "filterByStatusData")
    private Object[][] getFilterByStatusData() throws IOException {
        String path = "src/test/java/com/danaDeals/test/voucher/filterVoucherByStatus/TestDataFilterByStatus.xlsx";

        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1", 1);

        Object[][] filterByStatusData = new Object[rownum][colcount];

        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                filterByStatusData[i - 1][j] = Utility.getCellData(path, "Sheet1", i, j);
            }
        }

        return(filterByStatusData);
    }
}
