package com.danaDeals.test.voucher.showAllVoucher;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.Utility;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_ShowAllVoucherUser extends TestBase {

    @BeforeClass
    private void loggerInfo() {
        logger.info("********" +getClass().getName()+ "*********");
    }

    @Test(dataProvider = "showAllVoucherData")
    private void TC_ShowAllVoucherUser(String tcID, String page, String devStatus, String message,
                                       String httpStatus) {

        logger.info("******** TC_ShowAllVoucherUser_" + tcID + " ********");

        String loginToken = loadFile("token.txt");
        String invalidToken = loadFile("invalidToken.txt");

        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        if (Integer.parseInt(tcID) == 2) {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/user/show-all-voucher");

        } else if (Integer.parseInt(tcID) == 5) {
            httpRequest.header("Authorization", "Bearer " + invalidToken);
            response = httpRequest.request(Method.GET, "/api/user/show-all-voucher?" + page);

        } else if (Integer.parseInt(tcID) == 1) {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/user/show-all-voucher?" + page);

            //Check if in one page only have maximum 10 vouchers
            checkVoucherInAPage(getNumberOfVoucher());

        } else {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/user/show-all-voucher?" + page);

        }

        responseBody = response.getBody().asString();

        //check response body
        checkBody(message);
        checkPath("/api/user/show-all-voucher");
        checkTimestamp();

        //check status
        checkDevStatus(devStatus);
        checkStatusCode(httpStatus);

        //check response time
        checkResponseTime("3000");
    }

    @DataProvider(name = "showAllVoucherData")
    private Object[][] getShowAllVoucherData() throws IOException {
        String path = "src/test/java/com/danaDeals/test/voucher/showAllVoucher/TestDataShowAllVoucher.xlsx";

        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1", 1);

        Object[][] showAllVoucherData = new Object[rownum][colcount];

        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                showAllVoucherData[i - 1][j] = Utility.getCellData(path, "Sheet1", i, j);
            }
        }

        return(showAllVoucherData);
    }
}
