package com.danaDeals.test.voucher.filterVoucherByMerchantCategory;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.Utility;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_FilterVoucherByMerchantCategory extends TestBase {

    @BeforeClass
    private void loggerInfo() {
        logger.info("********" +getClass().getName()+ "*********");
    }

    @Test(dataProvider = "filterByMerchantData")
    private void TC_FilterByMerchantCategory(String tcID, String merchantCategory, String page, String devStatus,
                                             String message, String httpStatus, String path) {

        String loginToken = loadFile("token.txt");
        String invalidToken = loadFile("invalidToken.txt");

        logger.info("******** TC_FilterVoucherByMerchantCategory_" + tcID + " ********");

        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        if (Integer.parseInt(tcID) == 3) {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/user/filter-voucher?"+merchantCategory);

        } else if (Integer.parseInt(tcID) == 7) {
            httpRequest.header("Authorization", "Bearer " + invalidToken);
            response = httpRequest.request(Method.GET, "/api/user/filter-voucher?"+merchantCategory+"&"+page);

        } else if (Integer.parseInt(tcID) == 1) {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/user/filter-voucher?"+merchantCategory+"&"+page);

            //Check if in one page only have maximum 10 vouchers
            checkVoucherInAPage(getNumberOfVoucher());

        } else {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/user/filter-voucher?"+merchantCategory+"&"+page);

        }

        responseBody = response.getBody().asString();

        //check response body
        checkBody(message);
        checkPath(path);
        checkTimestamp();

        //check status
        checkDevStatus(devStatus);
        checkStatusCode(httpStatus);

        //check response time
        checkResponseTime("3000");
    }

    @DataProvider(name = "filterByMerchantData")
    private Object[][] getFilterByMerchantData() throws IOException {
        String path = "src/test/java/com/danaDeals/test/voucher/filterVoucherByMerchantCategory/TestDataFilterByMerchant.xlsx";

        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1", 1);

        Object[][] filterByMerchantData = new Object[rownum][colcount];

        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                filterByMerchantData[i - 1][j] = Utility.getCellData(path, "Sheet1", i, j);
            }
        }

        return(filterByMerchantData);
    }
}
