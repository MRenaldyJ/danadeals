package com.danaDeals.test.voucher.searchVoucherByMerchant;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.Utility;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_SearchVoucherByMerchantAdmin extends TestBase {

    @BeforeClass
    private void loggerInfo() {
        logger.info("********" +getClass().getName()+ "*********");
    }

    @Test(dataProvider = "searchByMerchantData")
    private void TC_SearchVoucherByMerchantAdmin(String tcID, String merchantName, String page, String devStatus,
                                                 String message, String httpStatus) {

        String loginToken = loadFile("adminToken.txt");
        String invalidToken = loadFile("invalidToken.txt");

        logger.info("******** TC_SearchMerchantAdmin_" + tcID + " ********");

        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        if (Integer.parseInt(tcID) == 6) {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/admin/findByMerchantName-voucher?merchantName="
                    +merchantName);

        } else if (Integer.parseInt(tcID) == 9) {
            httpRequest.header("Authorization", "Bearer " + invalidToken);
            response = httpRequest.request(Method.GET, "/api/admin/findByMerchantName-voucher?merchantName="
                    +merchantName+page);

        } else if (Integer.parseInt(tcID) == 1 || Integer.parseInt(tcID) == 2 || Integer.parseInt(tcID) == 4) {

            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/admin/findByMerchantName-voucher?merchantName="
                    +merchantName+page);

            //Check if in one page only have maximum 10 vouchers
            checkVoucherInAPage(getNumberOfVoucher());

            //Check if the vouchers that collected is similar to search keyword
            for (int i = 0; i < getNumberOfVoucher(); i++) {
                checkDataContains("data.content.merchantName[0]["+i+"]", merchantName.toLowerCase());
            }

        } else {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/admin/findByMerchantName-voucher?merchantName="
                    +merchantName+page);

        }

        responseBody = response.getBody().asString();

        //check response body
        checkBody(message);
        checkPath("/api/admin/findByMerchantName-voucher");
        checkTimestamp();

        //check status
        checkDevStatus(devStatus);
        checkStatusCode(httpStatus);

        //check response time
        checkResponseTime("3000");
    }

    @DataProvider(name = "searchByMerchantData")
    private Object[][] getSearchVoucherData() throws IOException {
        String path = "src/test/java/com/danaDeals/test/voucher/searchVoucherByMerchant/TestDataSearchByMerchant.xlsx";

        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1", 1);

        Object[][] searchByMerchantData = new Object[rownum][colcount];

        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                searchByMerchantData[i - 1][j] = Utility.getCellData(path, "Sheet1", i, j);
            }
        }

        return(searchByMerchantData);
    }
}
