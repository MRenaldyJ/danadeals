package com.danaDeals.test.voucher.createVoucher;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import com.danaDeals.utilities.Utility;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.simple.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_CreateVoucher extends TestBase {
    @BeforeClass
    private void loggerInfo() {
        logger.info("********" +getClass().getName()+ "*********");
    }

    @SuppressWarnings("unchecked")
    @Test(dataProvider = "createVoucherData")
    private void TC_CreateVoucher(String tcID, String idUser, String idMerchant, String voucherName,
                                  String voucherPrice, String discount, String maxDiscount, String quota,
                                  String expiredDate, String status, String devStatus, String message,
                                  String httpStatus, String path) {

        String loginToken = loadFile("adminToken.txt");
        String invalidToken = loadFile("invalidToken.txt");
        String voucherNameUnique = RestUtils.voucherNameGenerator();

        logger.info("******** TC_CreateVoucher_" + tcID + " ********");

        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        requestParams = new JSONObject();

        if (Integer.parseInt(tcID) == 8) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", Integer.parseInt(voucherName));
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 9) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", voucherPrice);
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 12) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", discount);
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 15) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", maxDiscount);
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 18) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", quota);
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 25) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", null);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 26) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", null);
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 27) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", null);
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 28) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", null);
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 29) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", null);
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 30) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", null);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 31) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", null);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 32) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", null);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", null);
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", null);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 33) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 34) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 35) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 36) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 37) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 38) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 39) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 40) {
            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 41) {
            httpRequest.header("Authorization", "Bearer " + invalidToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

        } else if (Integer.parseInt(tcID) == 1) {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherNameUnique);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");

            //Check is response data same with request data
            checkDataEquals("data[0].voucherName", voucherNameUnique);
            checkDataEquals("data[0].voucherPrice", voucherPrice);
            checkDataEquals("data[0].discount", discount);
            checkDataEquals("data[0].maxDiscount", maxDiscount);
            checkDataEquals("data[0].quota", quota);
            checkDataEquals("data[0].expiredDate", expiredDate);
            checkDataEquals("data[0].status", status);

        } else {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("voucherName", voucherName);
            requestParams.put("voucherPrice", Double.parseDouble(voucherPrice));
            requestParams.put("discount", Double.parseDouble(discount));
            requestParams.put("maxDiscount", Double.parseDouble(maxDiscount));
            requestParams.put("quota", Integer.parseInt(quota));
            requestParams.put("expiredDate", expiredDate);
            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.POST, "/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");
        }

        responseBody = response.getBody().asString();

        //check response body
        checkBody(message);
        checkPath(path);
        checkTimestamp();

        //check status
        checkDevStatus(devStatus);
        checkStatusCode(httpStatus);

        //check response time
        checkResponseTime("3000");
    }

    @DataProvider(name = "createVoucherData")
    private Object[][] getCreateVoucherData() throws IOException {
        String path = "src/test/java/com/danaDeals/test/voucher/createVoucher/TestDataCreateVoucher.xlsx";

        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1", 1);

        Object[][] createVoucherData = new Object[rownum][colcount];

        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                createVoucherData[i - 1][j] = Utility.getCellData(path, "Sheet1", i, j);
            }
        }

        return(createVoucherData);
    }
}
