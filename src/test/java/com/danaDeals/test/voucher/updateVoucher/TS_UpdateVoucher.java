package com.danaDeals.test.voucher.updateVoucher;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.Utility;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.simple.JSONObject;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_UpdateVoucher extends TestBase {

    @BeforeClass
    private void loggerInfo() {
        logger.info("********" +getClass().getName()+ "*********");
    }

    @SuppressWarnings("unchecked")
    @Test(dataProvider = "updateVoucherData")
    private void TC_UpdateVoucher(String tcID, String idVoucher, String status, String quota,
                                  String devStatus, String message, String httpStatus, String path) {

        String loginToken = loadFile("adminToken.txt");
        String invalidToken = loadFile("invalidToken.txt");

        logger.info("******** TC_UpdateVoucher_" + tcID + " ********");

        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        requestParams = new JSONObject();

        if (Integer.parseInt(tcID) == 1 || Integer.parseInt(tcID) == 2 || Integer.parseInt(tcID) == 4 ||
                Integer.parseInt(tcID) == 5) {

            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("status", status);

            httpRequest.body(requestParams.toJSONString());

        } else if (Integer.parseInt(tcID) == 9) {

            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("status", status);
            requestParams.put("quota", quota);

            httpRequest.body(requestParams.toJSONString());

        } else if (Integer.parseInt(tcID) == 13) {

            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("quota", Integer.parseInt(quota));

            httpRequest.body(requestParams.toJSONString());

        } else if (Integer.parseInt(tcID) == 14) {

            httpRequest.header("Authorization", "Bearer " + loginToken);
            httpRequest.header("Content-Type", "application/json");

        } else if (Integer.parseInt(tcID) == 15) {

            httpRequest.header("Authorization", "Bearer "+invalidToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("status", status);
            requestParams.put("quota", Integer.parseInt(quota));

            httpRequest.body(requestParams.toJSONString());

        } else {

            httpRequest.header("Authorization", "Bearer "+loginToken);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("status", status);
            requestParams.put("quota", Integer.parseInt(quota));

            httpRequest.body(requestParams.toJSONString());
        }

        response = httpRequest.request(Method.PUT, "/api/admin/update-status-voucher/"+idVoucher+"/restock");

        responseBody = response.getBody().asString();

        //check response body
        checkBody(message);
        checkPath(path);
        checkTimestamp();

        //check status
        checkDevStatus(devStatus);
        checkStatusCode(httpStatus);

        //check response time
        checkResponseTime("3000");
    }

    @DataProvider(name = "updateVoucherData")
    private Object[][] getUpdateVoucherData() throws IOException {
        String path = "src/test/java/com/danaDeals/test/voucher/updateVoucher/TestDataUpdateVoucher.xlsx";

        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1", 1);

        Object[][] updateVoucherData = new Object[rownum][colcount];

        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                updateVoucherData[i - 1][j] = Utility.getCellData(path, "Sheet1", i, j);
            }
        }

        return(updateVoucherData);
    }
}
