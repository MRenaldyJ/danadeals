package com.danaDeals.test.voucher.sortVoucher;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.Utility;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_SortVoucher extends TestBase {

    @BeforeClass
    private void loggerInfo() {
        logger.info("********" +getClass().getName()+ "*********");
    }

    @Test(dataProvider = "sortVoucherData")
    private void TC_SortVoucher(String tcID, String sort, String page, String devStatus,
                                String message, String httpStatus, String path) {

        String loginToken = loadFile("token.txt");
        String invalidToken = loadFile("invalidToken.txt");

        logger.info("******** TC_SortVoucher_" + tcID + " ********");

        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        if (Integer.parseInt(tcID) == 3 | Integer.parseInt(tcID) == 4) {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/user/sort-voucher?"+sort);

        } else if (Integer.parseInt(tcID) == 5) {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/user/sort-voucher?"+page);

        } else if (Integer.parseInt(tcID) == 8) {
            httpRequest.header("Authorization", "Bearer " + invalidToken);
            response = httpRequest.request(Method.GET, "/api/user/sort-voucher?"+sort+"&"+page);

        } else if (Integer.parseInt(tcID) == 1) {

            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/user/sort-voucher?"+sort+"&"+page);

            //Check if in one page only have maximum 10 vouchers
            checkVoucherInAPage(getNumberOfVoucher());

            //Check if sort by discount works fine
            checkSortingVoucher(1);

        } else if (Integer.parseInt(tcID) == 2) {

            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/user/sort-voucher?"+sort+"&"+page);

            //Check if in one page only have maximum 10 vouchers
            checkVoucherInAPage(getNumberOfVoucher());

            //Check if sort by discount works fine
            checkSortingVoucher(2);

        } else {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/user/sort-voucher?"+sort+"&"+page);

        }

        responseBody = response.getBody().asString();

        //check response body
        checkBody(message);
        checkPath(path);
        checkTimestamp();

        //check status
        checkDevStatus(devStatus);
        checkStatusCode(httpStatus);

        //check response time
        checkResponseTime("3000");
    }

    @DataProvider(name = "sortVoucherData")
    private Object[][] getSortVoucherData() throws IOException {
        String path = "src/test/java/com/danaDeals/test/voucher/sortVoucher/TestDataSortVoucher.xlsx";

        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1", 1);

        Object[][] sortVoucherData = new Object[rownum][colcount];

        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                sortVoucherData[i - 1][j] = Utility.getCellData(path, "Sheet1", i, j);
            }
        }

        return(sortVoucherData);
    }
}
