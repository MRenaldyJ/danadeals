package com.danaDeals.test.voucher.detailVoucher;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.Utility;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_DetailVoucher extends TestBase {

    @BeforeClass
    private void loggerInfo() {
        logger.info("********" +getClass().getName()+ "*********");
    }

    @Test(dataProvider = "detailVoucherData")
    private void TC_DetailVoucher(String tcID, String idVoucher, String devStatus, String message,
                                  String httpStatus, String path) {

        String loginToken = loadFile("adminToken.txt");
        String invalidToken = loadFile("invalidToken.txt");

        logger.info("******** TC_DetailVoucher_" + tcID + " ********");

        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        if (Integer.parseInt(tcID) == 4) {
            httpRequest.header("Authorization", "Bearer " + invalidToken);
            response = httpRequest.request(Method.GET, "/api/admin/voucher-detail-voucher/"+idVoucher);

        } else {
            httpRequest.header("Authorization", "Bearer " + loginToken);
            response = httpRequest.request(Method.GET, "/api/admin/voucher-detail-voucher/"+idVoucher);

        }

        responseBody = response.getBody().asString();

        //check response body
        checkBody(message);
        checkPath(path);
        checkTimestamp();

        //check status
        checkDevStatus(devStatus);
        checkStatusCode(httpStatus);

        //check response time
        checkResponseTime("3000");
    }

    @DataProvider(name = "detailVoucherData")
    private Object[][] getDetailVoucherData() throws IOException {
        String path = "src/test/java/com/danaDeals/test/voucher/detailVoucher/TestDataDetailVoucher.xlsx";

        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1", 1);

        Object[][] detailVoucherData = new Object[rownum][colcount];

        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                detailVoucherData[i - 1][j] = Utility.getCellData(path, "Sheet1", i, j);
            }
        }

        return(detailVoucherData);
    }
}
