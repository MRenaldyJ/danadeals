package com.danaDeals.test.order.payVoucherOrder;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.Utility;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;

public class TS_PayVoucherOrder extends TestBase {

    @BeforeClass
    private void loggerInfo() throws InterruptedException {
        logger.info("********" + getClass().getName() + "*********");
        Thread.sleep(5);
    }

    @DataProvider(name = "payVoucherData")
    private Object[][] getCreateVoucherData() throws IOException {
        String path = "src/test/java/com/danaDeals/test/order/payVoucherOrder/TestDataPayVoucherOrder.xlsx";

        int rownum = Utility.getRowCount(path, "Sheet1");
        int colcount = Utility.getCellCount(path, "Sheet1", 1);

        Object[][] payVoucherData = new Object[rownum][colcount];

        for (int i = 1; i <= rownum; i++) {
            for (int j = 0; j < colcount; j++) {
                payVoucherData[i - 1][j] = Utility.getCellData(path, "Sheet1", i, j);
            }
        }

        return(payVoucherData);
    }

    @Test(priority = 0, dataProvider = "payVoucherData")
    private void TC_PayVoucherInvalid(String tcID, String idTransactionData, String devStatus, String message,
                                      String httpStatus) {

        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String invalidToken = loadFile("invalidToken.txt");
        String idTransaction = loadFile("idTransaction.txt");

        if (Integer.parseInt(tcID) == 2) {

            idTransaction = idTransactionData;

            RestAssured.baseURI = BaseURI;
            httpRequest = RestAssured.given();

            httpRequest.header("Authorization", "Bearer " + token);
            httpRequest.header("Content-Type", "application/json");

            requestParams.put("idTransaction", idTransaction);

            httpRequest.body(requestParams.toJSONString());

            response = httpRequest.request(Method.PUT, "/api/user/"+idUser+"/transaction/voucher");
            responseBody = response.getBody().asString();

        } else if (Integer.parseInt(tcID) == 3) {

            idTransaction = idTransactionData;

            payOrderVoucher(idTransaction, idUser, token);

        } else if (Integer.parseInt(tcID) == 4) {

            RestAssured.baseURI = BaseURI;
            httpRequest = RestAssured.given();

            httpRequest.header("Authorization", "Bearer " + token);
            httpRequest.header("Content-Type", "application/json");

            response = httpRequest.request(Method.PUT, "/api/user/"+idUser+"/transaction/voucher");
            responseBody = response.getBody().asString();

        } else if (Integer.parseInt(tcID) == 5) {

            idUser = "999";

            payOrderVoucher(idTransaction, idUser, token);

        } else {

            payOrderVoucher(idTransaction, idUser, invalidToken);
        }

        //check response body
        checkBody(message);
        checkPath("/api/user/"+idUser+"/transaction/voucher");
        checkTimestamp();

        //check status
        checkDevStatus(devStatus);
        checkStatusCode(httpStatus);

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void TC_PayVoucherSuccess() {

        String idTransaction = loadFile("idTransaction.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        RestAssured.baseURI = BaseURI;
        httpRequest = RestAssured.given();

        httpRequest.header("Authorization", "Bearer " + token);
        requestParams.put("idTransaction", Integer.parseInt(idTransaction));

        httpRequest.header("Content-Type", "application/json");
        httpRequest.body(requestParams.toJSONString());

        response = httpRequest.request(Method.PUT, "/api/user/"+idUser+"/transaction/voucher");
        responseBody = response.getBody().asString();

        //check response body
        checkBody("Your payment is successfull.");
        checkPath("/api/user/"+idUser+"/transaction/voucher");
        checkTimestamp();

        //check status
        checkDevStatus("028");
        checkStatusCode("200");

        //check response time
        checkResponseTime("3000");

    }

    @Test(priority = 2)
    private void TC_PayVoucherExpired() throws InterruptedException {

        Thread.sleep(60000);

        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String idTransactionExpired = loadFile("idTransactionExpired.txt");

        payOrderVoucher(idTransactionExpired, idUser, token);

        //check response body
        checkBody("This transaction has expired.");
        checkPath("/api/user/"+idUser+"/transaction/voucher");
        checkTimestamp();

        //check status
        checkDevStatus("031");
        checkStatusCode("406");

        //check response time
        checkResponseTime("3000");
    }
}

