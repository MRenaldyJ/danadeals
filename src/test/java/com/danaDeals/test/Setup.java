package com.danaDeals.test;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class Setup extends TestBase {

    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);
        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking timestamp
        checkTimestamp();

        //checking path
        checkPath(getPathRegis());

        //checking response data
        checkRegisData(name,email,telephone,Integer.toString(getIdUser()));

        //checking response time
        checkResponseTime("3000");

    }

    @Test(priority = 1)
    private void loginPayer() throws JSONException {
        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone,password);
        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");
        setToken();
        writeFile(getToken(),"token.txt");

        //checking response body
        checkBody("You are logged in.");

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking login data
        checkLoginData(telephone,Integer.toString(getIdUser()));

        //checking path
        checkPath(getPathLogin());

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void loginAdmin() throws JSONException {
        String telephone = loadFile("telephoneAdmin.txt");
        String password = loadFile("passwordAdmin.txt");

        login(telephone,password);
        setToken();
        writeFile(getToken(),"adminToken.txt");

        //checking status code
        checkStatusCode("200");

        //checking timestamp
        checkTimestamp();

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 3)
    private void topUpSuccess() {

        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String amount = loadFile("topupAmount.txt");
        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bni");

        topUp(virtualNumber,amount,idUser,token);

        //checking response body
        checkBody("Your TOPUP is successful.");
        checkPath("/api/user/"+idUser+"/transaction/topup");
        checkTimestamp();

        //checking status code
        checkDevStatus("033");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 4)
    private void createOrderSuccess() throws JSONException {

        String idVoucher = loadFile("unlimitedVoucher.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher, idUser, token);

        setIdTransaction();
        writeFile(Integer.toString(getIdTransaction()), "idTransaction.txt");

        //checking response body
        checkBody("A new transaction has been created!");
        checkPath("/api/user/"+idUser+"/transaction/voucher");
        checkTimestamp();

        //checking status code
        checkDevStatus("052");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");

    }

    @Test(priority = 4)
    private void createOrderExpired() throws JSONException {

        String idVoucher = loadFile("unlimitedVoucher.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher, idUser, token);

        setIdTransaction();
        writeFile(Integer.toString(getIdTransaction()), "idTransactionExpired.txt");

        //checking response body
        checkBody("A new transaction has been created!");
        checkPath("/api/user/"+idUser+"/transaction/voucher");
        checkTimestamp();

        //checking status code
        checkDevStatus("052");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");

    }
}
