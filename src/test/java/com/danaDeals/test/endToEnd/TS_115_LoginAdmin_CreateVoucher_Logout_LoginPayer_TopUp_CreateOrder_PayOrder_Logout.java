package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import io.restassured.path.json.JsonPath;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_115_LoginAdmin_CreateVoucher_Logout_LoginPayer_TopUp_CreateOrder_PayOrder_Logout extends TestBase {

    @Test(priority = 0)
    private void loginAdmin() throws JSONException {

        String telephoneAdmin = loadFile("telephoneAdmin.txt");
        String passwordAdmin = loadFile("passwordAdmin.txt");

        login(telephoneAdmin, passwordAdmin);

        setToken();
        writeFile(getToken(), "adminToken.txt");

        //check response body
        checkBody("You are logged in.");
        checkPath("/api/auth/login");
        checkTimestamp();

        //check status
        checkDevStatus("010");
        checkStatusCode("200");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void createVoucherSuccess() {

        String voucherName = RestUtils.voucherNameGenerator();
        String voucherPrice = loadFile("voucherPrice.txt");
        String discount = loadFile("discount.txt");
        String maxDiscount = loadFile("maxDiscount.txt");
        String quota = loadFile("quota.txt");
        String expiredDate = loadFile("expiredDate.txt");
        String status = loadFile("status.txt");
        String idUser = loadFile("idUserAdmin.txt");
        String token = loadFile("adminToken.txt");
        String idMerchant = loadFile("idMerchant.txt");

        createVoucher(voucherName, voucherPrice, discount, maxDiscount, quota, expiredDate, status, idUser,
                idMerchant, token);

        JsonPath jsonPath = response.jsonPath();
        String idVoucher = jsonPath.get("data[0].idVoucher").toString();
        writeFile(idVoucher, "idVoucherTest.txt");

        //Check is response data same with request data
        checkDataEquals("data[0].voucherName", voucherName);
        checkDataEquals("data[0].voucherPrice", voucherPrice);
        checkDataEquals("data[0].discount", discount);
        checkDataEquals("data[0].maxDiscount", maxDiscount);
        checkDataEquals("data[0].quota", quota);
        checkDataEquals("data[0].expiredDate", expiredDate);
        checkDataEquals("data[0].status", status);

        //check response body
        checkBody("Create voucher successfully.");
        checkPath("/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");
        checkTimestamp();

        //check status
        checkDevStatus("042");
        checkStatusCode("201");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void logoutAdmin() {

        String idUser = loadFile("idUserAdmin.txt");
        String token = loadFile("adminToken.txt");

        logout(idUser, token);

        //checking response body
        checkBody("You are logged out.");
        checkPath("/api/user/"+idUser+"/logout");
        checkTimestamp();

        //checking status code
        checkDevStatus("041");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 3)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response data
        checkRegisData(name, email, telephone, Integer.toString(getIdUser()));

        //checking response body
        checkBody("Registration is successful.");
        checkPath("/api/auth/register");
        checkTimestamp();

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 4)
    private void loginSuccess() throws JSONException {

        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone, password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()), "idUser.txt");
        setToken();
        writeFile(getToken(), "token.txt");

        //checking login data
        checkLoginData(telephone, Integer.toString(getIdUser()));

        //checking response body
        checkBody("You are logged in.");
        checkPath("/api/auth/login");
        checkTimestamp();

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 5)
    private void createOrderSuccess() throws JSONException {

        String idVoucher = loadFile("idVoucherTest.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher, idUser, token);

        setIdTransaction();
        writeFile(Integer.toString(getIdTransaction()), "idTransaction.txt");

        //checking response body
        checkBody("A new transaction has been created!");
        checkPath("/api/user/"+idUser+"/transaction/voucher");
        checkTimestamp();

        //checking status code
        checkDevStatus("052");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");

    }

    @Test(priority = 6)
    private void topUpSuccess() {

        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String amount = loadFile("topupAmount.txt");
        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bni");

        topUp(virtualNumber,amount,idUser,token);

        //checking response body
        checkBody("Your TOPUP is successful.");
        checkPath("/api/user/"+idUser+"/transaction/topup");
        checkTimestamp();

        //checking status code
        checkDevStatus("033");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 7)
    private void payOrderSuccess() {

        String idTransaction = loadFile("idTransaction.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        payOrderVoucher(idTransaction, idUser, token);

        //checking response body
        checkBody("Your payment is successfull.");
        checkPath("/api/user/"+idUser+"/transaction/voucher");
        checkTimestamp();

        //checking status code
        checkDevStatus("028");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 8)
    private void logoutSuccess() {

        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        logout(idUser, token);

        //checking response body
        checkBody("You are logged out.");
        checkPath("/api/user/"+idUser+"/logout");
        checkTimestamp();

        //checking status code
        checkDevStatus("041");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }
}
