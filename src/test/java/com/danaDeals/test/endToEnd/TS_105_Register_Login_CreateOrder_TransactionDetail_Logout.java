package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_105_Register_Login_CreateOrder_TransactionDetail_Logout extends TestBase {

    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password, password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response data
        checkRegisData(name, email, telephone, Integer.toString(getIdUser()));

        //checking response body
        checkBody("Registration is successful.");
        checkPath("/api/auth/register");
        checkTimestamp();

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void loginSuccess() throws JSONException {

        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone, password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()), "idUser.txt");
        setToken();
        writeFile(getToken(), "token.txt");

        //checking login data
        checkLoginData(telephone, Integer.toString(getIdUser()));

        //checking response body
        checkBody("You are logged in.");
        checkPath("/api/auth/login");
        checkTimestamp();

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void createOrderSuccess() throws JSONException {

        String idVoucher = loadFile("unlimitedVoucher.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher, idUser, token);

        setIdTransaction();
        writeFile(Integer.toString(getIdTransaction()), "idTransaction.txt");

        //checking response body
        checkBody("A new transaction has been created!");
        checkPath("/api/user/"+idUser+"/transaction/voucher");
        checkTimestamp();

        //checking status code
        checkDevStatus("052");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");

    }

    @Test(priority = 3)
    private void transactionDetail() {

        String idTransaction = loadFile("idTransaction.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        transactionDetail(idTransaction, idUser, token);

        //check response body
        checkBody("Transaction history are successfully collected.");
        checkPath("/api/user/"+idUser+"/transaction/"+idTransaction);
        checkTimestamp();

        //check status
        checkDevStatus("039");
        checkStatusCode("200");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 4)
    private void logoutSuccess() {

        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        logout(idUser, token);

        //checking response body
        checkBody("You are logged out.");
        checkPath("/api/user/"+idUser+"/logout");
        checkTimestamp();

        //checking status code
        checkDevStatus("041");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }
}
