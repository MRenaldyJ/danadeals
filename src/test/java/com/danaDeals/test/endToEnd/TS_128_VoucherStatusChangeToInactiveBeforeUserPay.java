package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_128_VoucherStatusChangeToInactiveBeforeUserPay extends TestBase {

    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");
        checkPath("/api/auth/register");
        checkTimestamp();

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void loginSuccess() throws JSONException {

        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone, password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()), "idUser.txt");
        setToken();
        writeFile(getToken(), "token.txt");

        //checking response body
        checkBody("You are logged in.");
        checkPath("/api/auth/login");
        checkTimestamp();

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void topUpSuccess() {

        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String amount = loadFile("topupAmount.txt");
        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bni");

        topUp(virtualNumber,amount,idUser,token);

        //checking response body
        checkBody("Your TOPUP is successful.");
        checkPath("/api/user/"+idUser+"/transaction/topup");
        checkTimestamp();

        //checking status code
        checkDevStatus("033");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 3)
    private void createOrderSuccess() throws JSONException {

        String idVoucher = "26";
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher, idUser, token);

        setIdTransaction();
        writeFile(Integer.toString(getIdTransaction()), "idTransaction.txt");

        //checking response body
        checkBody("A new transaction has been created!");
        checkPath("/api/user/"+idUser+"/transaction/voucher");
        checkTimestamp();

        //checking status code
        checkDevStatus("052");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");

    }

    @Test(priority = 4)
    private void loginAdmin() throws JSONException {

        String telephoneAdmin = "+6287800000000";
        String passwordAdmin = "P@ssw0rd";

        login(telephoneAdmin, passwordAdmin);

        setIdUser();
        writeFile(Integer.toString(getIdUser()), "idUserAdmin.txt");
        setToken();
        writeFile(getToken(), "adminToken.txt");

        //check response body
        checkBody("You are logged in.");
        checkPath("/api/auth/login");
        checkTimestamp();

        //check status
        checkDevStatus("010");
        checkStatusCode("200");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 5)
    private void updateVoucherStatus() {

        String idVoucher = "26";
        String token = loadFile("adminToken.txt");

        updateVoucher(1, "false", "empty", idVoucher, token);

        //check response body
        checkBody("Successfully change status.");
        checkPath("/api/admin/update-status-voucher/"+idVoucher+"/restock");
        checkTimestamp();

        //check status
        checkDevStatus("044");
        checkStatusCode("200");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 6)
    private void logoutAdmin() {

        String idUser = loadFile("idUserAdmin.txt");
        String token = loadFile("adminToken.txt");

        logout(idUser, token);

        //checking response body
        checkBody("You are logged out.");
        checkPath("/api/user/"+idUser+"/logout");
        checkTimestamp();

        //checking status code
        checkDevStatus("041");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 7)
    private void payOrderFailed() {

        String idTransaction = loadFile("idTransaction.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        payOrderVoucher(idTransaction, idUser, token);

        //checking response body
        checkBody("The voucher is currently not available.");
        checkPath("/api/user/"+idUser+"/transaction/voucher");
        checkTimestamp();

        //checking status code
        checkDevStatus("027");
        checkStatusCode("404");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 8)
    private void logoutSuccess() {

        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        logout(idUser, token);

        //checking response body
        checkBody("You are logged out.");
        checkPath("/api/user/"+idUser+"/logout");
        checkTimestamp();

        //checking status code
        checkDevStatus("041");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }
}
