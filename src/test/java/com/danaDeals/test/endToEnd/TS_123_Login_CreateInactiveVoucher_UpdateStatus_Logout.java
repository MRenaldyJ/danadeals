package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import io.restassured.path.json.JsonPath;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_123_Login_CreateInactiveVoucher_UpdateStatus_Logout extends TestBase {

    @Test(priority = 0)
    private void loginAdmin() throws JSONException {

        String telephoneAdmin = loadFile("telephoneAdmin.txt");
        String passwordAdmin = loadFile("passwordAdmin.txt");

        login(telephoneAdmin, passwordAdmin);

        setToken();
        writeFile(getToken(), "adminToken.txt");

        //check response body
        checkBody("You are logged in.");
        checkPath("/api/auth/login");
        checkTimestamp();

        //check status
        checkDevStatus("010");
        checkStatusCode("200");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void createVoucherSuccess() {

        String voucherName = RestUtils.voucherNameGenerator();
        String voucherPrice = loadFile("voucherPrice.txt");
        String discount = loadFile("discount.txt");
        String maxDiscount = loadFile("maxDiscount.txt");
        String quota = loadFile("quota.txt");
        String expiredDate = loadFile("expiredDate.txt");
        String status = "false";
        String idUser = loadFile("idUserAdmin.txt");
        String token = loadFile("adminToken.txt");
        String idMerchant = loadFile("idMerchant.txt");

        createVoucher(voucherName, voucherPrice, discount, maxDiscount, quota, expiredDate, status, idUser,
                idMerchant, token);

        JsonPath jsonPath = response.jsonPath();
        String idVoucher = jsonPath.get("data[0].idVoucher").toString();
        writeFile(idVoucher, "idVoucherTest.txt");

        //check response body
        checkBody("Create voucher successfully.");
        checkPath("/api/admin/"+idUser+"/merchant/"+idMerchant+"/vouchers");
        checkTimestamp();

        //check status
        checkDevStatus("042");
        checkStatusCode("201");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void updateVoucherStatus() {

        String idVoucher = loadFile("idVoucherTest.txt");
        String token = loadFile("adminToken.txt");

        updateVoucher(1, "true", "empty", idVoucher, token);

        //check response body
        checkBody("Successfully change status.");
        checkPath("/api/admin/update-status-voucher/"+idVoucher+"/restock");
        checkTimestamp();

        //check status
        checkDevStatus("044");
        checkStatusCode("200");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 3)
    private void logoutAdmin() {

        String idUser = loadFile("idUser.txt");
        String token = loadFile("adminToken.txt");

        logout(idUser, token);

        //checking response body
        checkBody("You are logged out.");
        checkPath("/api/user/"+idUser+"/logout");
        checkTimestamp();

        //checking status code
        checkDevStatus("041");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }
}
