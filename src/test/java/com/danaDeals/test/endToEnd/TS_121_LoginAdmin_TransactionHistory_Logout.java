package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_121_LoginAdmin_TransactionHistory_Logout extends TestBase {

    @Test(priority = 0)
    private void loginAdmin() throws JSONException {

        String telephoneAdmin = loadFile("telephoneAdmin.txt");
        String passwordAdmin = loadFile("passwordAdmin.txt");

        login(telephoneAdmin, passwordAdmin);

        setIdUser();
        writeFile(Integer.toString(getIdUser()), "idUser.txt");
        setToken();
        writeFile(getToken(), "adminToken.txt");

        //check response body
        checkBody("You are logged in.");
        checkPath("/api/auth/login");
        checkTimestamp();

        //check status
        checkDevStatus("010");
        checkStatusCode("200");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void transactionHistoryFailed() {

        String page = "0";
        String idUser = loadFile("idUser.txt");
        String token = loadFile("adminToken.txt");

        transactionHistory("empty", "empty", "empty", page, idUser, token);

        //check response body
        checkBody("You are not authorized.");
        checkPath("/api/user/"+idUser+"/transaction");
        checkTimestamp();

        //check status
        checkDevStatus("021");
        checkStatusCode("401");

        //check response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void logoutAdmin() {

        String idUser = loadFile("idUser.txt");
        String token = loadFile("adminToken.txt");

        logout(idUser, token);

        //checking response body
        checkBody("You are logged out.");
        checkPath("/api/user/"+idUser+"/logout");
        checkTimestamp();

        //checking status code
        checkDevStatus("041");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }
}
