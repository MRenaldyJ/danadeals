package com.danaDeals.test.endToEnd;

import com.danaDeals.base.TestBase;
import com.danaDeals.utilities.RestUtils;
import org.json.JSONException;
import org.testng.annotations.Test;

public class TS_111_Register_Login_CreateOrder_PayOrder_TopUp_PayOrder extends TestBase {

    @Test(priority = 0)
    private void registerSuccess() throws JSONException {
        String email = RestUtils.gmailGenerator();
        String telephone = RestUtils.telephoneNumber();
        String name = RestUtils.nameGenerator();
        String password = RestUtils.passwordGenerator();

        register(name, email, telephone, password,password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()),"idUser.txt");

        //checking response body
        checkBody("Registration is successful.");
        checkPath("/api/auth/register");
        checkTimestamp();

        //checking status code
        checkDevStatus("001");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 1)
    private void loginSuccess() throws JSONException {

        String telephone = loadFile("telephone.txt");
        String password = loadFile("password.txt");

        login(telephone, password);

        setIdUser();
        writeFile(Integer.toString(getIdUser()), "idUser.txt");
        setToken();
        writeFile(getToken(), "token.txt");

        //checking response body
        checkBody("You are logged in.");
        checkPath("/api/auth/login");
        checkTimestamp();

        //checking status code
        checkDevStatus("010");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 2)
    private void createOrderSuccess() throws JSONException {

        String idVoucher = loadFile("unlimitedVoucher.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        createOrderVoucher(idVoucher, idUser, token);

        setIdTransaction();
        writeFile(Integer.toString(getIdTransaction()), "idTransaction.txt");

        //checking response body
        checkBody("A new transaction has been created!");
        checkPath("/api/user/"+idUser+"/transaction/voucher");
        checkTimestamp();

        //checking status code
        checkDevStatus("052");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");

    }

    @Test(priority = 3)
    private void payOrderFailed() {

        String idTransaction = loadFile("idTransaction.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        payOrderVoucher(idTransaction, idUser, token);

        //checking response body
        checkBody("Your balance is not enough.");
        checkPath("/api/user/"+idUser+"/transaction/voucher");
        checkTimestamp();

        //checking status code
        checkDevStatus("029");
        checkStatusCode("402");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 4)
    private void topUpSuccess() {

        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");
        String amount = loadFile("topupAmount");
        String telephone = loadFile("telephone.txt");
        String virtualNumber = RestUtils.virtualNumberGenerator(telephone,"bni");

        topUp(virtualNumber,amount,idUser,token);

        //checking response body
        checkBody("Your TOPUP is successful.");
        checkPath("/api/user/"+idUser+"/transaction/topup");
        checkTimestamp();

        //checking status code
        checkDevStatus("033");
        checkStatusCode("201");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 5)
    private void payOrderSuccess() {

        String idTransaction = loadFile("idTransaction.txt");
        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        payOrderVoucher(idTransaction, idUser, token);

        //checking response body
        checkBody("Your payment is successfull.");
        checkPath("/api/user/"+idUser+"/transaction/voucher");
        checkTimestamp();

        //checking status code
        checkDevStatus("028");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }

    @Test(priority = 6)
    private void logoutSuccess() {

        String idUser = loadFile("idUser.txt");
        String token = loadFile("token.txt");

        logout(idUser, token);

        //checking response body
        checkBody("You are logged out.");
        checkPath("/api/user/"+idUser+"/logout");
        checkTimestamp();

        //checking status code
        checkDevStatus("041");
        checkStatusCode("200");

        //checking response time
        checkResponseTime("3000");
    }
}
